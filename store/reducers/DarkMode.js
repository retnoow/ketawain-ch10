import { createSlice } from '@reduxjs/toolkit'

export const DarkMode = createSlice({
  name: 'darkMode',
  initialState: {
    value: false,
  },
  reducers: {

    toggleDarkMode: (state, action) => {
      state.value = action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const { toggleDarkMode } = DarkMode.actions

export default DarkMode.reducer