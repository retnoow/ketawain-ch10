import { createSlice } from '@reduxjs/toolkit'

export const DarkMode = createSlice({
  name: 'auth',
  initialState: {
    isAuthenticated: false,
    is
  },
  reducers: {

    toggleDarkMode: (state, action) => {
      state.value = action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const { toggleDarkMode } = DarkMode.actions

export default DarkMode.reducer