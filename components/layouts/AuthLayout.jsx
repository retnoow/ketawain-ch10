import React from 'react'
import Navigation from '../Navigation'
import { useSelector, useDispatch } from "react-redux";

const AuthLayout = ({children}) => {
  const darkMode = useSelector((state) => state.darkMode.value);

  return (
    <div className={`${darkMode ? "bg-dark-1" : "bg-light-3"} min-vh-100`}>
        <Navigation />
        {children}
    </div>
  )
}

export default AuthLayout