import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { Col, Form, Row } from "react-bootstrap";
import { updateDoc, doc, onSnapshot } from "firebase/firestore";
import firebaseApp from "../config/firebaseConfig";
import { getFirestore } from "firebase/firestore";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen } from "@fortawesome/free-solid-svg-icons";

const Update = (item) => {
  const [show, setShow] = useState(false);

  const id = item.itemId;
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const router = useRouter();
  const [detail, setDetail] = useState(null);
  const [loading] = useState(false);

  const getDB = getFirestore(firebaseApp);

  const [jokes, setJokes] = useState({
    username: item.item.username,
    description: item.item.description,
    imageUrl: item.item.imageUrl,
    likes: item.item.likes,
  });

  useEffect(() => {
    const view = doc(getDB, "jokes", id);
    onSnapshot(view, (snapshot) => {
      setDetail({ ...snapshot.data(), id: snapshot.id });
    });
  }, [getDB, id]);

  const UpdateHandler = async () => {
    const jokesRef = doc(getDB, "jokes", detail.id);

    try {
      await updateDoc(jokesRef, { ...jokes, likes: item.item.likes });

      setShow(false);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <Button variant="outline-primary" onClick={handleShow}>
        <FontAwesomeIcon icon={faPen} />
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Express your jokes here, buddy!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Row>
              <Col md={6}>
                <Form.Label>Your AKA</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Write your AKA"
                  onChange={(e) =>
                    setJokes({ ...jokes, username: e.target.value })
                  }
                  defaultValue={jokes.username}
                />
              </Col>
              <Col md={6}>
                <Form.Label>Image Url</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter image URL"
                  onChange={(e) =>
                    setJokes({ ...jokes, imageUrl: e.target.value })
                  }
                  defaultValue={jokes.imageUrl}
                />
              </Col>
              <Col md={12}>
                <Form.Label>Jokes</Form.Label>
                <Form.Control
                  as="textarea"
                  placeholder="Write your jokes"
                  style={{ height: "100px" }}
                  onChange={(e) =>
                    setJokes({ ...jokes, description: e.target.value })
                  }
                  defaultValue={jokes.description}
                />
              </Col>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            disabled={loading}
            onClick={() => UpdateHandler()}
          >
            {loading ? "Loading" : "Post"}
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Update;
