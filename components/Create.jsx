import React, {useState} from 'react'
import { Col, Row, Form } from 'react-bootstrap'
import { Modal, Button } from 'react-bootstrap'
import { useAuthState } from 'react-firebase-hooks/auth'
import { getAuth } from 'firebase/auth'
import firebaseApp from '../config/firebaseConfig'
import { doc, setDoc, collection, getFirestore } from 'firebase/firestore'

const Create = () => {

    const [show, setShow] = useState(false)
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const [jokes, setJokes] = useState({
        username: '',
        description: '',
        imageUrl: '',
        likes:[]
    })

    const [loading, setLoading] = useState(false)

    const createDoc = async () => {
        const db= getFirestore(firebaseApp)
        const jokesRef= collection(db, 'jokes')
        try {
            setLoading(true)

            if(!jokes.username || !jokes.description || !jokes.imageUrl){
                window.confirm("Please, complete all the fields")            
            }
            else{
                await setDoc(doc(jokesRef),{...jokes, })
                window.confirm("Your jokes is posted")
                
            }      
        } catch (err) {
            console.log(err.message)
        }
        setLoading(false)
        handleClose(false)
    }

  return (
    <div>
        <Button variant="primary" onClick={handleShow}>
            Create Jokes
        </Button>
        
        <Modal show={show} >
            <Modal.Header closeButton onClick={handleClose}>
                <Modal.Title>Express your jokes here, buddy!</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row className='justify-content-center'>
                    <Form className='mt-3'>
                        <Row className='g-3'>
                            <Col md={6}>
                                <Form.Label>Your AKA</Form.Label>
                                <Form.Control type="text" placeholder="Write your AKA"
                                    onKeyDown={(e) => e.key === 'Enter' && createDoc() && setJokes('')}
                                    onChange={(e) => setJokes({ ...jokes, username: e.target.value})} value={jokes.username} />
                            </Col>
                            <Col md={6}>
                                <Form.Label>Image Url</Form.Label>
                                <Form.Control type="text" placeholder="Enter image URL" 
                                    onKeyDown={(e) => e.key === 'Enter' && createDoc() && setJokes('')}
                                    onChange={(e) => setJokes({ ...jokes, imageUrl: e.target.value})} value={jokes.imageUrl} />
                            </Col>
                            <Col md={12}>
                                <Form.Label>Jokes</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    placeholder="Write your jokes"
                                    style={{ height: '100px' }}
                                    onKeyDown={(e) => e.key === 'Enter' && createDoc() && setJokes('')}
                                    onChange={(e) => setJokes({ ...jokes, description: e.target.value})} value={jokes.description} />
                            </Col>
                        </Row>
                    </Form>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>Close</Button>
                <Button variant="primary" disabled={loading} onClick={() => {createDoc() && setJokes('')}}>
                    {loading ? 'Loading' : 'Post' }</Button>
            </Modal.Footer>
        </Modal>

    </div>
  )
}

export default Create