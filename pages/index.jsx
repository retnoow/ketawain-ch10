import Navigation from "../components/Navigation";
import Carousel from "../components/Carousel";
import { useSelector, useDispatch } from "react-redux";

export default function Home() {
  const darkMode = useSelector((state) => state.darkMode.value);

  return (
    <div className={`${darkMode ? "bg-dark-1" : "bg-light-3"}`}>
      <Navigation />

      <h1 className={`text-center m-4 ${darkMode ? "text-light" : "text-dark"}`}>Homepage</h1>
      <Carousel />
    </div>
  );
}
