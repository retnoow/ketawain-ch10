import React from "react";
import { Container, Row, Col, Button, Form, Spinner } from "react-bootstrap";
import { useState, useEffect } from "react";
import { useCreateUserWithEmailAndPassword } from "react-firebase-hooks/auth";
import { getAuth, signOut } from "firebase/auth";
import firebaseApp from "../config/firebaseConfig";
import "bootstrap/dist/css/bootstrap.min.css";
import Router, { useRouter } from "next/router";
import AuthLayout from "../components/layouts/AuthLayout";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";


const Register = () => {

  const auth = getAuth(firebaseApp);
  const router = useRouter();
  const darkMode = useSelector((state) => state.darkMode.value);

  const [credentials, setCredentials] = useState({
    username: "",
    email: "",
    password: "",
  });

  const [createUserWithEmailAndPassword, user, loading, error] =
    useCreateUserWithEmailAndPassword(auth);


  useEffect(() => {
      if(user !== undefined)
        router.push('/login')
        signOut(auth)
  }, [user, auth, router]);

  const registerHandler =  async () =>{
    await createUserWithEmailAndPassword(
      credentials.email,
      credentials.password
    ) 
  }

  return (
    <AuthLayout>
      <Container className={`d-flex justify-content-center align-items-center ` } style={{height: '90vh'}}>
        <Row>
          <Col
            style={{ height: "auto" }}
            className={`d-flex justify-content-center align-items-center flex-column gap-4 text-center bg-light-3 text-secondary ${darkMode ? "bg-dark-1" : "bg-light-3"}`}
          >
            <h2>Kenapa di keyboard komputer ada tombol ENTER?</h2>
            <h2>Karena kalo tulisannya ENTAR, programnya nggak jalan-jalan</h2>
          </Col>
          <Col
            className={`d-flex justify-content-center flex-column gap-3 bg-light-2 rounded p-5 ${darkMode ? "bg-dark-2" : "bg-light-1"}`}
            style={{ height: "auto" }}
          >
            <h1 className="mb-4">Register</h1>
            <Form>
              <Form.Group className="mb-3 fs-5" controlId="formBasicUsername">
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type="username"
                  placeholder="Enter username"
                  onChange={(e) =>
                    setCredentials({ ...credentials, username: e.target.value })
                  }
                  onKeyDown={(e) => e.key === "Enter" && registerHandler()}
                  className="form-control"
                  value={credentials.username}
                />
              </Form.Group>
              <Form.Group className="mb-3 fs-5" controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email address"
                  onChange={(e) =>
                    setCredentials({ ...credentials, email: e.target.value })
                  }
                  onKeyDown={(e) => e.key === "Enter" && registerHandler()}
                  className="form-control"
                  value={credentials.email}
                />
              </Form.Group>

              <Form.Group className="mb-3 fs-5" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={(e) =>
                    setCredentials({ ...credentials, password: e.target.value })
                  }
                  onKeyDown={(e) => e.key === "Enter" && registerHandler()}
                  className="form-control"
                  value={credentials.password}
                />
              </Form.Group>
              <div className="d-flex justify-content-end ml-2 mt-2 mb-5">
                <Button
                  disabled={loading}
                  className=""
                  variant="primary"
                  type="button"
                  onClick={registerHandler}
                >
                  {loading && (
                    <Spinner
                      as="span"
                      animation="grow"
                      size="sm"
                      role="status"
                      aria-hidden="true"
                    />
                  )}
                  {loading ? "Loading..." : "Register"}
                </Button>
              </div>

              <span className="d-flex justify-content-center align-items-center">Sudah punya akun KETAWAIN? &nbsp;<Link  href="/login"><a className="text-decoration-none text-success">Masuk</a></Link></span>
            </Form>
          </Col>
        </Row>
      </Container>
    </AuthLayout>
  );
};

export default Register;
